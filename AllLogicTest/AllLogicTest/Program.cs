﻿using System;

namespace AllLogicTest
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("---Logic---");
            new Program();
            Console.ReadKey();
        }

        public Program()
        {
            string done = "T";
            while(done.ToUpper() == "T")
            {
                Console.WriteLine("1. Kalkulator");                
                Console.WriteLine("2. Bilangan Prima");                
                Console.Write("Masukkan pilihan = ");
                int pilihan = int.Parse(Console.ReadLine());
                switch (pilihan)
                {
                    case 1:
                        new Kalkulator();
                        break; 
                    case 2:
                        new AlgoritmaBilanganPrima();
                        break;
                    default:
                        Console.WriteLine("Error pilihan tidak ada");
                        break;
                }

                Console.Write("Keluar ? [Y/T] ");
                done = Console.ReadLine();    
                Console.Clear();
            }
        }       
    }
}
