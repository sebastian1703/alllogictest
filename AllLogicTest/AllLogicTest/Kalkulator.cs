﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AllLogicTest
{
    public class Kalkulator
    {
        public Kalkulator()
        {
            string done = "T";
            while (done.ToUpper() == "T")
            {
                Console.WriteLine("===Kalkulator===");
                Console.WriteLine("1. Penjumlahan");
                Console.WriteLine("2. Pengurangan");
                Console.WriteLine("3. Perkalian");
                Console.WriteLine("4. Pembagian");
                Console.WriteLine("5. Pemangkatan");
                Console.Write("Masukkan pilihan = ");
                int pilihan = int.Parse(Console.ReadLine());
                switch (pilihan)
                {
                    case 1:
                        Penjumlahan();
                        break;
                    case 2:
                        Pengurangan();
                        break;
                    case 3:
                        Perkalian();
                        break;
                    case 4:
                        Pembagian();
                        break;
                    case 5:
                        Pemangkatan();
                        break;
                    default:
                        Console.WriteLine("Error pilihan tidak ada");
                        break;
                }

                Console.Write("Keluar ? [Y/T] ");
                done = Console.ReadLine();
                Console.Clear();
            }
        }

        public void Penjumlahan()
        {
            Console.Write("Masukkan nilai a = ");
            int a = int.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai b = ");
            int b = int.Parse(Console.ReadLine());

            Console.WriteLine(a + b);
        }

        public void Pengurangan()
        {
            Console.Write("Masukkan nilai a = ");
            int a = int.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai b = ");
            int b = int.Parse(Console.ReadLine());

            Console.WriteLine(a - b);
        }

        public void Perkalian()
        {
            Console.Write("Masukkan nilai a = ");
            int a = int.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai b = ");
            int b = int.Parse(Console.ReadLine());

            Console.WriteLine(a * b);
        }

        public void Pembagian()
        {
            Console.Write("Masukkan nilai a = ");
            int a = int.Parse(Console.ReadLine());
            Console.Write("Masukkan nilai b = ");
            int b = int.Parse(Console.ReadLine());

            Console.WriteLine(a / b);
        }

        public void Pemangkatan()
        {
            Console.Write("Masukkan nilai a = ");
            int a = int.Parse(Console.ReadLine());
            Console.Write("Masukkan pangkat = ");
            int b = int.Parse(Console.ReadLine());

            Console.WriteLine(Math.Pow(a, b));
        }
    }
}
